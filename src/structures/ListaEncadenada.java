package structures;

import java.util.Iterator;


public class ListaEncadenada<T>{

	private NodoSimple<T> primero;
	private NodoSimple<T> ultimo;
	private NodoSimple<T> actual;
	private int tamLista;

	public ListaEncadenada(){
		tamLista = 0;
		primero = null;
		ultimo = primero;
		actual = primero;
	}
	public Iterator<T> iterator() {
		return new Iterator<T>(){
			NodoSimple<T> nuevo = primero;

			public boolean hasNext() {
				if(primero == null)
					return false;
				if(nuevo.darSiguiente()== null)
					return false;
				else
					return true;	
			}
			public T next() {
				T elem = null;
				if(nuevo != null){
					elem = nuevo.darItem();
					nuevo=nuevo.darSiguiente();
				}
				return elem;
			}
			
			public void remove() {
				// TODO Auto-generated method stub
				
			}

		};
	}

	public T darElementoSiguiente() {
		if(actual.darSiguiente()!=null)
		{
		actual= actual.darSiguiente();
		return actual.darItem();
		}
		else 
		{
			actual=null;
			return null;
		}
	}
	public void agregarElementoAlInicio(T item)
	{
		NodoSimple<T> nuevo = new NodoSimple<>(item);
		NodoSimple<T> actual = primero;
		if(actual == null)
		{
			primero = nuevo;
		}
		else
		{
			nuevo.cambiarSiguiente(primero);
			primero= nuevo;
		}
	}
	
	public void agregarElemento(T elem) {
		NodoSimple<T> act = new NodoSimple<T>(elem);
		if(primero == null){
			primero = act;
			actual = primero;
			ultimo = primero;
		}else{
			ultimo = darUltimo();
			ultimo.cambiarSiguiente(act);
		}
		tamLista++;

	}

	public NodoSimple<T> darUltimo(){
		NodoSimple<T> actual = primero;
		while(actual.darSiguiente()!= null)
		{
			actual = actual.darSiguiente();
		}
		return actual;
	}

	public T darElemento(int pos) {
		T resp=null;
		int contador=0;
		NodoSimple<T> actual= primero;
		while(actual!=null)
		{
			T ob= actual.darItem();
			if (contador==pos) {
				resp=ob;
				break;
			}
			actual=actual.darSiguiente();
			contador++;
		}
		return resp;
	}


	
	public int darNumeroElementos() {
		return tamLista;
	}
	public T darElementoPosicionActual() {
		return actual.darItem();
	}


	public boolean avanzarSiguientePosicion() {
		NodoSimple<T> sig = actual;
		if(sig == null)
			return false;
		else
			actual = sig.darSiguiente();
		return true;
	}


	public boolean retrocederPosicionAnterior() {
		NodoSimple<T> ant = actual;
		if(primero.darSiguiente() == null)
			return false;
		if((ant.darSiguiente()).darSiguiente()!= null)
			ant = ant.darSiguiente();
		return true;
	}
	public void borrarElementos()
	{
		if(primero.darSiguiente()!=null)
		{
        primero.cambiarSiguiente(null);
        primero = null;
		}
		else 
			primero = null;
		
        tamLista=0;
	}
	public T eliminarElemento(int pos)
	{
		NodoSimple<T> n = null;
		if(primero!=null)
		{
			if(darElemento(pos)!=null)
			{
				if( pos == 0)
				{
					if(actual == primero)
					{
						actual = primero.darSiguiente();
					}
					// Es EL primer elemento de la lista
					n=primero;
					primero = primero.darSiguiente( );
					return n.darItem();
				}
				else
				{
					n = darElementoNodoSimple(pos);
					if(actual == darElementoNodoSimple(pos))
					{
						actual = actual.darSiguiente();
					}
					NodoSimple<T> anterior = darElementoNodoSimple(pos-1);
					if(anterior!=null)
					{
						darElementoNodoSimple(pos-1).desconectarSiguiente();
					}
					return n.darItem();
				}
			}
		}
		return null;
	}	
	
	public NodoSimple<T> darElementoNodoSimple(int pos) 
	{
		int contador = 0;
		NodoSimple<T> actual = primero;
		while (actual!=null) 
		{
			if(contador == pos)
			{
				return actual;
			}
			contador++;
			actual = actual.darSiguiente();
		}
		return null;
	}
	public void cambiarPrimero(NodoSimple<T> nodo){
		primero = nodo;
	}
	public NodoSimple<T> darPrimero(){
		return primero;
	}
	public void reiniciarActual() {
		actual = primero;
		
	}
	public static class NodoSimple<T>{

		private NodoSimple<T> siguiente;
		private T item;

		public NodoSimple( T elem){
			item = elem;
			siguiente = null;
		}

		public T darItem(){
			return item;
		}
		public NodoSimple<T> darSiguiente(){
			return siguiente;
		}
		public void cambiarSiguiente(NodoSimple<T> nodo){
			siguiente = nodo;
		}
		public void desconectarSiguiente()
		{
			siguiente = siguiente.siguiente; 
		}
	}


}