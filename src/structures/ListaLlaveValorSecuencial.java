package structures;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class ListaLlaveValorSecuencial<K ,V> 
{

	/**
	 * Atributo que modela la cabeza de la lista
	 */
	private NodoSimpleListaValor<K,V> cabezaNodo;

	/**
	 * Atributo que modela el nodoActual
	 */
	private NodoSimpleListaValor<K,V> nodoActual;

	/**
	 * Inserta el nodo en la lista
	 * @param llave
	 * @param valor
	 */
	public void insertar(K llave, V valor) 
	{	
		boolean borrarNodo=false;
		if(valor==null)
			borrarNodo=true;
		NodoSimpleListaValor<K,V> nuevo = new NodoSimpleListaValor<K,V>(llave, valor) ;
		NodoSimpleListaValor<K,V> actual = cabezaNodo;
		if(cabezaNodo == null)
		{
			cabezaNodo = nuevo;
			nodoActual = cabezaNodo;
		}
		else
		{			
			int contador=0;
			boolean agregado=false;
			while(actual!=null)
			{				
				if(actual.darLlave().equals(llave))
				{
					if(!borrarNodo)
					{
						actual.setValor(valor);
						agregado=true;
					}
					else
					{
						eliminarNodo(contador);
						agregado=true;
					}
					break;    					
				}
				actual = actual.darSiguiente();
				contador++;
			}	
			actual=cabezaNodo;
			while(actual!=null&&actual.darSiguiente()!=null)
			{
				actual=actual.darSiguiente();			
			}

			if(!agregado)
			{
				//System.out.println("No estaba agregado "+llave+", se agreg� el nuevo");
				actual.cambiarSiguiente(nuevo);
			}
		}

	}

	/**
	 * Retorna la llave K en la pos dada
	 * @param pos
	 * @return k llave
	 */
	public K darLlave(int pos) 
	{
		int contador = 0;
		NodoSimpleListaValor<K,V> actual = cabezaNodo;
		while (actual!=null) 
		{
			if(contador == pos)
			{
				return actual.darLlave();
			}
			contador++;
			actual = actual.darSiguiente();
		}
		return null;
	}

	/**
	 * Retorna el valor V dada la llave
	 * @param llave
	 * @return valor V
	 */
	public V darValor(K llave)
	{
		NodoSimpleListaValor<K,V> actual = cabezaNodo;
		while (actual!=null) 
		{
			if(actual.darLlave().equals(llave))
			{
				return actual.darValor();
			}			
			actual = actual.darSiguiente();
		}
		return null;

	}

	/**
	 * Retorna true si esta vac�a
	 * @return true si esta vac�a, false de lo contrario
	 */
	public boolean estaVacia()
	{
		if(cabezaNodo==null)
			return true;
		else
			return false;
	}

	/**
	 * Retorna el numero de elementos en la lista
	 * @return numero de elementos
	 */
	public int darTamanio() 
	{
		int numeroElementos = 0;
		if(cabezaNodo!=null)
		{
			NodoSimpleListaValor<K,V> actual = cabezaNodo;
			while (actual!=null) 
			{
				numeroElementos++;
				actual = actual.darSiguiente();
			}
		}
		return numeroElementos;
	}

	/**
	 * Retorna la llave del nodo actual
	 * @return k llave actual
	 */
	public K darLlavePosicionActual() 
	{
		if(nodoActual!=null)
		{
			return nodoActual.darLlave();
		}
		return null;
	}

	/**
	 * Avanza a la siguiente posicion y retorna true si se pudo
	 * @return true si se logro avanzar de posicion, false de lo contrario
	 */
	public boolean avanzarSiguientePosicion() 
	{
		if(nodoActual!=null && nodoActual.darSiguiente()!=null)
		{
			nodoActual = nodoActual.darSiguiente();
			return true;
		}
		return false;
	}

	/**
	 * Cambia el actual a la cabeza
	 */
	public void cambiarActualACabeza()
	{
		nodoActual = cabezaNodo;
	}

	/**
	 * Constructor de la clase
	 */
	public ListaLlaveValorSecuencial()
	{
		cabezaNodo=null;
		nodoActual=cabezaNodo;
	}

	/**
	 * Retorna truem, si tiene siguiente, false de lo contrario
	 * @return true si tiene siguiente
	 */
	public boolean tieneSiguiente()
	{
		if(nodoActual.darSiguiente()!=null)
		{
			return true;
		}
		return false;
	}

	/**
	 * Retorna la cabeza de la lista
	 * @return
	 */
	public NodoSimpleListaValor<K,V> darCabezaNodo()
	{
		return cabezaNodo;
	}

	/**
	 * Retorna el nodo dada la posici�n
	 * @param pos en donde se encuentra el nodo
	 * @return el nodo que se encuentra en la posici�n nada, null si no se encuentra
	 */
	public NodoSimpleListaValor<K,V> darElementoNodoSimpleListaValor(int pos) 
	{
		int contador = 0;
		NodoSimpleListaValor<K,V> actual = cabezaNodo;
		while (actual!=null) 
		{
			if(contador == pos)
			{
				return actual;
			}
			contador++;
			actual = actual.darSiguiente();
		}
		return null;
	}

	/**
	 * Retorna la posici�n del nodo actual en la lista
	 * @return pos del elemento actual, -1 si no se encontro nodo
	 */
	public int darPosActual()
	{
		if(nodoActual.darLlave().equals(cabezaNodo.darLlave()))
		{
			return 0;
		}
		else
		{
			NodoSimpleListaValor<K,V> n =cabezaNodo.darSiguiente();
			int pos=1;
			while(n!=null)
			{
				if(n.darLlave().equals(nodoActual.darLlave()))
				{
					return pos;
				}
				pos++;
			}
		}
		return -1;
	}

	/**
	 * Elimina el elemento que se encuentra en la posici�n dada por par�metro
	 * @param pos del elemento a eliminar
	 * @return elmento del nodo eliminado
	 */
	public K eliminarNodo(int pos)
	{
		NodoSimpleListaValor<K,V> n = null;
		if(cabezaNodo!=null)
		{
			if(darLlave(pos)!=null)
			{
				if( pos == 0)
				{
					if(nodoActual == cabezaNodo)
					{
						nodoActual = nodoActual.darSiguiente();
					}
					// Es EL primer elemento de la lista
					n=cabezaNodo;
					cabezaNodo = cabezaNodo.darSiguiente( );
					return n.darLlave();
				}
				else
				{
					n = darElementoNodoSimpleListaValor(pos);
					if(nodoActual == darElementoNodoSimpleListaValor(pos))
					{
						nodoActual = nodoActual.darSiguiente();
					}
					NodoSimpleListaValor<K,V> anterior = darElementoNodoSimpleListaValor(pos-1);
					if(anterior!=null)
					{
						darElementoNodoSimpleListaValor(pos-1).desconectarSiguiente();
					}
					return n.darLlave();
				}
			}
		}
		return null;
	}
	
	/**
	 * Elimina el elemento que se encuentra la llave dada por par�metro
	 * @param llave del elemento a eliminar
	 * @return elmento del nodo eliminado
	 */
	public K eliminarNodo(K llave)
	{
		int pos = darPosLlave(llave);
		if(pos<0)
		{
			return null;
		}
		NodoSimpleListaValor<K,V> n = null;
		if(cabezaNodo!=null)
		{
			if(darLlave(pos)!=null)
			{
				if( pos == 0)
				{
					if(nodoActual == cabezaNodo)
					{
						nodoActual = nodoActual.darSiguiente();
					}
					// Es EL primer elemento de la lista
					n=cabezaNodo;
					cabezaNodo = cabezaNodo.darSiguiente( );
					return n.darLlave();
				}
				else
				{
					n = darElementoNodoSimpleListaValor(pos);
					if(nodoActual == darElementoNodoSimpleListaValor(pos))
					{
						nodoActual = nodoActual.darSiguiente();
					}
					NodoSimpleListaValor<K,V> anterior = darElementoNodoSimpleListaValor(pos-1);
					if(anterior!=null)
					{
						darElementoNodoSimpleListaValor(pos-1).desconectarSiguiente();
					}
					return n.darLlave();
				}
			}
		}
		return null;
	}
	
	/**
	 * Retorna la pos en la que se encuentra una llave
	 * @param llave
	 * @return pos de la llave, -1 si no se encuentra
	 */
	public int darPosLlave(K llave)
	{
		NodoSimpleListaValor<K, V> nodo = cabezaNodo;
		int pos = 0;
		while(nodo != null)
		{
			if(nodo.darLlave().equals(llave))
			{
				return pos;
			}
			pos++;
			nodo = nodo.darSiguiente();
		}
		return -1;
	}

	/**
	 * Agrega un elemento al inicio de la lista
	 * @param item del nuevo nodo
	 */
	public void agregarElementoAlInicio(NodoSimpleListaValor<K,V> nuevo1)
	{
		NodoSimpleListaValor<K,V> nuevo = nuevo1;
		NodoSimpleListaValor<K,V> actual = cabezaNodo;
		if(actual == null)
		{
			cabezaNodo = nuevo;
		}
		else
		{
			nuevo.setNext(cabezaNodo);
			cabezaNodo = nuevo;
		}
	}

	/**
	 * Retorna true si tiene una llave K
	 * @param llave
	 * @return true si tiene , false de lo contrario
	 */
	public boolean tieneLlave(K llave)
	{
		//nodoActual=cabezaNodo;
		NodoSimpleListaValor<K,V> actual = cabezaNodo;
		if(cabezaNodo == null)
		{
			return false;
		}
		else if(cabezaNodo.darLlave().equals(llave))
		{
			return true;
		}
		else
		{
			while(actual!=null)
			{
				if(actual.darLlave().equals(llave))
				{
					return true;   					
				}
				actual = actual.darSiguiente();
			}	
			return false;
		}
	}

	/**
	 * Iterable de llaves K
	 * @return iterable
	 */
	public Iterable<K> llaves()
	{
		return new Iterable<K>()
		{
			@Override
			public Iterator<K> iterator() 
			{
				return llavesK();
			}
		};		
	}
	
	/**
	 * Iterator usado en Iterable llaves()
	 * @return iterator de llaves
	 */
	public Iterator<K> llavesK()
	{
	    final ListaLlaveValorSecuencial<K, V> lista = this;
	    return new Iterator<K>() 
	    {
	        final NodoSimpleListaValor<K,V> primero = lista.cabezaNodo;
	        NodoSimpleListaValor<K,V> actualNodo = null;
	        @Override
	        public boolean hasNext() 
	        {
	            if (lista.estaVacia())
	            {
	                return false;
	            } 
	            else if (actualNodo == null)
	            {
	                return true;
	            } 
	            else if (actualNodo == lista.darElementoNodoSimpleListaValor(darTamanio()-1))
	            {
	                return false;
	            }
	            return true;
	        }
	        @Override
	        public K next() 
	        {
	            if (lista.estaVacia())
	            {
	                throw new NoSuchElementException();
	            } 
	            else if (actualNodo == null)
	            {
	                this.actualNodo = primero;
	                return actualNodo.darLlave();
	            } 
	            else if (actualNodo.darSiguiente() == null) 
	            {
	                throw new NoSuchElementException();
	            }
	            this.actualNodo = actualNodo.darSiguiente();
	            return actualNodo.darLlave();
	        }
			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
	    };
	}
	public static class NodoSimpleListaValor<K ,V> 
	{
		/**
		 * Atributo que modela el elemento T del nodo
		 */
		private K llave ;
		
		private V valor;

		/**
		 * Atributo que modela el siguiente nodo
		 */
		private NodoSimpleListaValor<K,V> siguiente;

		/**
		 * Retorna el siguiente nodo
		 * @return
		 */
		public NodoSimpleListaValor<K,V> darSiguiente()
		{
			return siguiente;
		}

		/**
		 * Cambia el siguiente
		 * @param i, nodo cambiar por el siguiente
		 */
		public void cambiarSiguiente(NodoSimpleListaValor<K,V> i)
		{
			i.siguiente = siguiente;
			siguiente = i;
		}
		

		/**
		 * Contructore de la clase, se crea el nodo con el elemento T dado por parámetro
		 * @param t del nodo a crear
		 */
		public NodoSimpleListaValor(K key, V val) 
		{
			llave=key;
			valor=val;
			siguiente = null;
		}
		
		public void setValor(V val)
		{
			valor=val;
		}
		
		public K darLlave()
		{
			return llave;
		}
		
		public V darValor()
		{
			return valor;
		}
		
		public void setNext(NodoSimpleListaValor<K,V> n)
		{
			this.siguiente = n;
		}

		/**
		 * Desconecta el siguiente nodo
		 */
		public void desconectarSiguiente()
		{
			siguiente = siguiente.siguiente; 
		}
	}

}