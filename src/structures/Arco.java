package structures;

public class Arco {	
	
	private int verOrigen;
	private int verFin;
	private double peso;
	private char ordenLexi;
	
	public Arco(int verticeOrigen, int verticeFinal, double pPeso){
		
	verOrigen = verticeOrigen;
	verFin = verticeFinal;
	peso = pPeso;
	
	}
	
	/**
	 * retorna el nodo de salida de la arista
	 * @return nodoSalida
	 */
	public int darVerticeOrigen(){
		return verOrigen;
	}
	
	/**
	 * Setter
	 * @return
	 */
	public void cambiarVerticeOrigen(int nuevo){
		verOrigen = nuevo;
	}
	
	/**
	 * Setter
	 * @return
	 */
	public int darVerticeFin(){
		return verFin;
	}
	/**
	 * retorna el nodo de llegada de la arista
	 * @return nodoLlegada
	 */
	public void cambiarVerticeFin(int nuevo){
		verFin = nuevo;
	}
	/**
	 * Retorna el peso del arco entre el vertice origen y el vertice final
	 * @return
	 */
	public double darPeso(){
		return peso;
	}
	/**
	 * Setter
	 * @param nuevo
	 */
	public void cambiarPeso(double nuevo){
		peso = nuevo;
	}
	public char darOrdenLexi(){
		return ordenLexi;
	}
	public void cambiarOrdenLexi(char nuevo){
		ordenLexi = nuevo;
	}
	
}
