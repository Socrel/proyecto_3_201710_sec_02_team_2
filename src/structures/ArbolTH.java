package structures;

public class ArbolTH <K extends Comparable<K>,V>{
	
	/**
	 * Modela la capacidad incicial que debe tener la tabla;
	 */
	
	private static final int CAPACIDAD_INICIAL = 4;
	/**
	 * Modela la tabla 
	 */
	private ArbolLLaveValorSecuencuencial<K,V>[] tabla;

	/**
	 * Modela el tamagno
	 */
	private int tamagno;

	/**
	 * Modela el numero de parejas key - valos
	 */
	private int n;

	/**
	 * Crea el encadenamiento separado
	 * @param m cadenas
	 */
	public ArbolTH(int m)
	{
		this.tamagno = m;
		tabla = (ArbolLLaveValorSecuencuencial<K,V>[]) new ArbolLLaveValorSecuencuencial[m]; 
		for(int i = 0 ; i < m; i++)
		{
			tabla[i] = new ArbolLLaveValorSecuencuencial<K,V>();
		}	
	}

	/**
	 * Hace un rehash dado el n�mero de cadenas
	 */
	public void ajustarTamagno(int cadenas)
	{
		ArbolTH<K, V> temp = new ArbolTH<K,V>(cadenas);
		for (int i = 0; i < tamagno; i++) 
		{
			for (K key : tabla[i].keys()) 
			{
				temp.insertar(key, tabla[i].get(key));
			}
		}
		this.tamagno  = temp.tamagno;
		this.n  = temp.n;
		this.tabla = temp.tabla;
	}

	/**
	 * Retorna el numero de elementos
	 * @return # elementos
	 */
	public int darTamanio()
	{
		return n;
	}

	/**
	 * Retorna true si esta vac�a, false de lo contrario
	 * @return
	 */
	public boolean estaVacia()
	{
		return darTamanio()  == 0;
	}

	/**
	 * Retorna true si la tabla contiene una llave dada por parametro
	 * @param llave
	 * @return true si la contiene, false de lo contrario
	 */
	public boolean tieneLlave(K llave)
	{
		return darValor(llave)  != null;
	}

	/**
	 * Retorna el valor V, dada la llave
	 * @param llave
	 * @return valor V
	 */
	public V darValor(K llave)
	{
		if(llave == null)
		{
			return null;
		}
		else
		{
			int i = hash(llave);
			return tabla[i].get(llave);
		}
	}

	/**
	 * Inserta un nuevo elemento con su llave K, y valor V
	 * @param llave
	 * @param valor
	 */
	public void insertar(K llave, V valor)
	{
		if (llave == null)
		{
			throw new IllegalArgumentException("Se intento agregar un elemento con llave null");
		}
		if (valor == null) 
		{
			eliminar(llave);
			return;
		}
		// si el tamagno promedio de listas es mayor o igual que diez, se duplica el tamagno de la tabla
		if (n >= 10*tamagno)
		{
			ajustarTamagno(2*tamagno);
		}
		int i = hash(llave);
		if (tabla[i].contains(llave)) 
		{
			n++;
		}
		tabla[i].put(llave, valor);

	}

	/**
	 * Removes the specified key and its associated value from this symbol table     
	 * (if the key is in this symbol table).    
	 *
	 * @param  key the key
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void eliminar(K llave) 
	{
		if (llave == null)
		{
			return;
		}

		int i = hash(llave);
		if (tabla[i].contains(llave))
		{
			n--;
		}
		tabla[i].delete(llave);

		// disminuir longitud de la tabla si el promedio de listas es <= 2
		if (tamagno > CAPACIDAD_INICIAL && n <= 2*tamagno)
		{
			ajustarTamagno(tamagno/2);
		}
	} 

	/**
	 * Retorna la longitud de las listas
	 * @return longitud de las listas en la tabla
	 */
	public int[] darLongitudListas()
	{
		int[] tama=new int[tamagno];

		for(int i=0;i<tabla.length;i++)
		{
			tama[i]=tabla[i].height();
		}
		return tama;
	}

	/**
	 * Genera una posicion dada por el hash de una llave
	 * @param key
	 * @return int posicion
	 */
	private int hash(K key)
	{ 
		return (key.hashCode() & 0x7fffffff) % tamagno;
	}

	/**
	 * Iterable de llaves K
	 * @return iterable
	 */
	public Iterable<K> llaves()
	{
		return listaDeTodasLosElementos().keys();
	}

	/**
	 * Retorna la tabla de hash
	 * @return tabla 
	 */
	public ArbolLLaveValorSecuencuencial<K, V>[] darTabla()
	{
		return tabla;
	}
	/**
	 * Crea una lista con todos los elementos en la tabla
	 * @return
	 */
	public ArbolLLaveValorSecuencuencial<K, V> listaDeTodasLosElementos()
	{
		ArbolLLaveValorSecuencuencial<K, V> arbol = new ArbolLLaveValorSecuencuencial<K,V>();
		for (int i = 0; i < tabla.length; i++) 
		{
			if(i == 0)
			{
				arbol = tabla[0];
			}
			else
			{
				if(tabla[i].max() != null)
				arbol.put(tabla[i].max(), tabla[i].get(tabla[i].max()));
			}
		}
		return arbol;
	}

}
