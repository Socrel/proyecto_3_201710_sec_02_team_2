package structures;

import structures.ListaEncadenada.NodoSimple;



public class Stack<T extends Comparable<T>> 
{
	/**
	 * Atributo que modelar� la lista en la que se encontrar� la lista
	 */
	private ListaEncadenada<T> listaPila; 
	
	/**
	 * Constructor de la clase, la lista se incicializa vac�a
	 */
	public Stack() 
	{
		listaPila = new ListaEncadenada<>();
	}

	public void push(T item)
	{
		listaPila.agregarElementoAlInicio(item);
	}
	
	public T pop()
	{
		return listaPila.eliminarElemento(0);
	}
	

	public boolean isEmpty()
	{
		if(size()==0)
		{
			return true;
		}
		return false;
	}
	

	public int size()
	{
		return listaPila.darNumeroElementos();
	}
	
	/**
	 * Retorna el primer elemento en la pila
	 * @return el primer elemento
	 */
	public NodoSimple<T> darPrimero()
	{
		return listaPila.darPrimero();
	}
	
	/**
	 * Retorna el �ltimo elemento en la pila
	 * @return �ltimo elemento
	 */
	public NodoSimple<T> darUltimo()
	{
		NodoSimple<T> actual = listaPila.darPrimero();
		while(actual!=null)
		{
			if(actual.darSiguiente() == null)
			{
				return actual;
			}
			actual = actual.darSiguiente();
		}
		return null;
	}
	
	/**
	 * Retorna el elemento actual en la pila
	 * @return elemento actual
	 */
	public NodoSimple<T> darActual()
	{
		return listaPila.darPrimero();
	}

}
