package structures;

import java.util.Iterator;

// K son los arcos de los vertices 
public class Grafo {
	private int numVertices;
	private int numArcos;
	private RedBlackBST <Integer,Arco>[] adjacentes;



	//RECORDAR!! los arreglos tienen la pocicion  hasta n-1
	public Grafo(int numVertices){
		this.numVertices= numVertices;
		numArcos=0;
		adjacentes= (RedBlackBST <Integer,Arco>[]) new RedBlackBST[numVertices];

		for(int v =0;v<numVertices;v++){
			adjacentes[v]= new RedBlackBST <Integer,Arco>();
		}
	}
	public int numeroVertices(){
		return numVertices;
	}
	public int numeroArcos(){
		return numArcos;
	}

	public RedBlackBST <Integer,Arco>[] darAdjacentes(){
		return adjacentes;
	}
	public RedBlackBST <Integer,Arco> darArbolAdjcDe(Integer vertice){
		if(vertice>=0&&vertice<numVertices){
		return adjacentes[vertice];
		}
		return null;
	}
	

	public void agregarArco(int pOrigen, int pDestino, double pPeso){
		if(pOrigen>=0&& pOrigen<numVertices && pDestino>=0&& pDestino<numVertices){
			Arco nuevo = new Arco(pOrigen,pDestino,pPeso);
			int tamanioA=adjacentes[pOrigen].size();
			adjacentes[pOrigen].put(pDestino, nuevo);
			if(adjacentes[pOrigen].size()>tamanioA)
				numArcos++;
		}
		else{
			System.out.println("Se detecto un error en los identificadores de los vertices");
		}
	}


	public Iterator<Integer> darVertices(){
		return new IteratorGrafo(); 
	}

	//iterador sobre los destinos de la lista de adjacencia del nodo id
	public Iterator<Integer> darVerticesAdyacentes(Integer id) {
		if(id>=0&&id<numVertices){
			return adjacentes[id].keys().iterator();
		}
		return null;
	}
	public int darNumeroVertices()
	{
		return numVertices;
	}
	public  int darNumeroArcos(){
		return numArcos;
	}

	private class IteratorGrafo implements Iterator<Integer>{
		private int contador;

		public  IteratorGrafo(){
			contador =0;
		}

		public boolean hasNext() {
			if(contador<numVertices){
				contador++;
				return true;
			}
			return false;
		}

		public Integer next() {
			return contador;
		}

		@Override
		public void remove() {
			System.out.println("El metodo de remove sobre grafos no esta soportado");			
		}

	}

}