package structures;

import structures.ListaEncadenada.NodoSimple;

public class Queue<T> {

	ListaEncadenada<T> lista;

	public boolean isEmpty() {
		return lista.darNumeroElementos()== 0;
	}

	public int size() {
		
		return lista.darNumeroElementos();
		
	}
	public void enqueue(T item) 
	{
		lista.agregarElemento(item);
	}

	public T dequeue() 
	{
		NodoSimple<T> primero = lista.darPrimero();
		T item = primero.darItem();
		lista.cambiarPrimero(primero.darSiguiente());
		return item;
	}

}
