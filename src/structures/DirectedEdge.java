package structures;

public class DirectedEdge <K> implements Comparable <DirectedEdge<K>>
{
	private final K v;
	private final K w;
	private final double weight;

	public DirectedEdge(K v, K w, double weight)
	{
		if (Double.isNaN(weight)) throw new IllegalArgumentException("Weight is NaN");
		this.v = v;
		this.w = w;
		this.weight = weight;
	}

	public K from() {
		return v;
	}

	public K to() {
		return w;
	}

	public double weight() {
		return weight;
	}

	public String toString() {
		return v + "->" + w + " " + String.format("%5.2f", weight);
	}

	public NodoCamino<K> camino (int lenght, double peso){
		return new NodoCamino <K> (w , v, peso + weight, lenght);
	}

	
	public int compareTo(DirectedEdge<K> arg) {
		if (weight == arg.weight)
			return 0;
		return (weight <= arg.weight)? -1:1;
	}
}
