package structures;

public class NodoCamino <K> implements Comparable <NodoCamino<K>>{

	public K idFinal;
	public K idAdy;
	public double weight;
	public int lenght;

	public NodoCamino (K idFinal, K idAdy, double weight, int length){
		this.idFinal = idFinal;
		this.idAdy = idAdy;
		this.weight = weight;
		this.lenght = length;
	}

	public String toString (){
		return  idAdy + "->" + idFinal + " - Peso: " + weight + " - Longitud: " + lenght; 
	}


	public int compareTo(NodoCamino<K> arg) {
		if (weight == arg.weight)
			return 0;
		return (weight <= arg.weight)? -1:1;
	}
}
