package structures;

import java.util.NoSuchElementException;

public class NodeVertice <K extends Comparable <K>,V>
{
	public K key;
	public V item;
	public RedBlackBST<Character, ListaEncadenada <DirectedEdge<K>>> edges;
	public int grado;
	public boolean marcado;
	
	public NodeVertice (K key, V item){
		edges = new RedBlackBST<Character, ListaEncadenada < DirectedEdge<K>>> ();
		this.item = item;
		this.key = key;
		this.grado = 0;
		this.marcado = false;
	}
	
	public int agregaEdge (K idOrigen, K idDestino, double peso, char ordenLexicografico){
		DirectedEdge<K> edge = new DirectedEdge<K>(idOrigen, idDestino, peso);
		
		 ListaEncadenada < DirectedEdge<K>> lista = null;
		try {
			lista = edges.get(ordenLexicografico);
		} catch (NoSuchElementException e){
			lista = new ListaEncadenada < DirectedEdge<K>>();
			edges.put(ordenLexicografico, lista);
		}		
		 int n = lista.darNumeroElementos();
		 grado += n;
		 return n;
	}
	
	
	
	public String toString (){
		return "LLave: " + key.toString() + "\tValor: " + item.toString() + "\tGrado: " + grado;
	}
}
