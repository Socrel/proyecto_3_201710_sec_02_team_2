package model.logic;

import java.io.FileReader;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.csvreader.CsvReader;

import structures.EncadenamientoSeparadoTH;
import structures.Grafo;
import structures.ListaEncadenada;
import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VORating;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import API.IEdge;
import API.ILista;
import API.ISistemaRecomendacion;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacion {

	public EncadenamientoSeparadoTH<String, VOTeatro > teatros = new EncadenamientoSeparadoTH<>(50);
	public EncadenamientoSeparadoTH<Integer, VOPelicula > peliculasTotal = new EncadenamientoSeparadoTH<>(50);
	public EncadenamientoSeparadoTH<String, VOGeneroPelicula> listaGeneros = new EncadenamientoSeparadoTH<String,VOGeneroPelicula>(30);
	public Grafo red;
	public EncadenamientoSeparadoTH<Integer, VOUsuario> usuarios = new EncadenamientoSeparadoTH<>(50);


	public ISistemaRecomendacion crearSR() {
		ISistemaRecomendacion sistema = new SistemaRecomendacionPeliculas();
		return sistema;
	}

	public boolean cargarTeatros(String ruta) {
		JSONParser parser = new JSONParser();
		try{
			JSONArray array = (JSONArray)parser.parse(new FileReader(ruta));

			int i=0;
			for(Object obj: array ){

				JSONObject jObject = (JSONObject) obj;

				String nombre = (String)jObject.get("Nombre");

				String ubicacion = (String) jObject.get("Ubicacion geografica (Lat|Long)");
				ubicacion = ubicacion.replaceAll("\"", "");
				String[] ubicacionlg = ubicacion.split("\\|");

				String latitud = ubicacionlg[0];
				String longitud = ubicacionlg[1];

				String franquicia = (String)jObject.get("Franquicia");

				VOTeatro teatro = new VOTeatro();
				VOUbicacion ubi = new VOUbicacion();
				VOFranquicia fra = new VOFranquicia();

				teatro.setNombre(nombre);
				ubi.setLatitud(Double.parseDouble(latitud));
				ubi.setLongitud(Double.parseDouble(longitud));
				teatro.setUbicacion(ubi);
				fra.setNombre(franquicia);
				teatro.setFranquicia(fra);
				teatro.setPosicion(i);
				teatros.insertar(nombre,teatro);
				i++;
			}return true;
		}catch(Exception e){
			System.out.println("Error cargar teatros");
			return false;
		}
	}

	public boolean cargarCartelera(String ruta1, String ruta2, String ruta3, String ruta4, String ruta5, String rutaM) {

		boolean ruta1Leida = false;
		boolean ruta2Leida = false;
		boolean ruta3Leida = false;
		boolean ruta4Leida = false;
		boolean ruta5Leida = false;
		int contador = 0;
		EncadenamientoSeparadoTH<Integer,EncadenamientoSeparadoTH<String,ListaEncadenada<String>>> dia = new EncadenamientoSeparadoTH<Integer,EncadenamientoSeparadoTH<String,ListaEncadenada<String>>>(50);
		EncadenamientoSeparadoTH<String,ListaEncadenada<String>> teatros = new EncadenamientoSeparadoTH<String,ListaEncadenada<String>>(5);
		ListaEncadenada<String> horas = new ListaEncadenada<String>();

		while(!ruta1Leida | !ruta2Leida | !ruta3Leida | !ruta4Leida | !ruta5Leida ){
			JSONParser parser = new JSONParser();
			try{
				JSONArray array = null;

				if(!ruta1Leida){
					array = (JSONArray)parser.parse(new FileReader(ruta1));
					ruta1Leida = true;
					contador++;
				}
				else if(!ruta2Leida){
					array = (JSONArray)parser.parse(new FileReader(ruta2));
					ruta2Leida = true;
					contador++;
				}
				else if(!ruta3Leida){
					array = (JSONArray)parser.parse(new FileReader(ruta3));
					ruta3Leida = true;
					contador++;
				}
				else if (!ruta4Leida){
					array = (JSONArray)parser.parse(new FileReader(ruta4));
					ruta4Leida = true;
					contador++;
				}
				else {
					array = (JSONArray)parser.parse(new FileReader(ruta5));
					ruta5Leida = true;
					contador++;
				}
				for(Object obj: array ){

					JSONObject jObject = (JSONObject) obj;

					String nombreT = (String) jObject.get("teatros");
					JSONObject teatro = (JSONObject)jObject.get("teatro");
					JSONArray peliculas = (JSONArray)teatro.get("peliculas");

					for(Object obj2 : peliculas){
						JSONObject jObject2 = (JSONObject) obj2;
						long id = (long)jObject2.get("id");
						JSONArray funciones = (JSONArray)jObject2.get("funciones");

						for(Object obj3 : funciones){
							JSONObject jObject3 = (JSONObject) obj3;

							String hora = (String)jObject3.get("hora");

							String ids = id+"";
							if(peliculasTotal.tieneLlave(Integer.parseInt(ids)))
							{
								VOPelicula actual = peliculasTotal.darValor(Integer.parseInt(ids));

								if(actual.getDia().tieneLlave(contador)){
									teatros = actual.getDia().darValor(contador);
									if(actual.getDia().darValor(contador).tieneLlave(nombreT))
										horas = actual.getDia().darValor(contador).darValor(nombreT);
									else
										horas = new ListaEncadenada<String>();
								}
								else
									teatros = new EncadenamientoSeparadoTH<String,ListaEncadenada<String>>(5);
								horas = new ListaEncadenada<String>();

								horas.agregarElemento(hora);
								teatros.insertar(nombreT,horas );

								dia = actual.getDia();
								dia.insertar(contador, teatros);

								actual.setDia(dia);
								actual.setTeatros(teatros);
								actual.setHoras(horas);
								peliculasTotal.insertar(Integer.parseInt(ids), actual);
							}
							else{
								VOPelicula actual = new VOPelicula();
								actual.setId(Integer.parseInt(ids));
								if(actual.getDia().tieneLlave(contador))
								{
									teatros = actual.getDia().darValor(contador);
									if(actual.getDia().darValor(contador).tieneLlave(nombreT))
										horas = actual.getDia().darValor(contador).darValor(nombreT);
									else
										horas = new ListaEncadenada<String>();
								}
								else
								{
									teatros = new EncadenamientoSeparadoTH<String,ListaEncadenada<String>>(5);
									horas = new ListaEncadenada<String>();
								}
								horas.agregarElemento(hora);
								teatros.insertar(nombreT,horas );

								dia = actual.getDia();
								dia.insertar(contador, teatros);

								actual.setDia(dia);
								actual.setTeatros(teatros);
								actual.setHoras(horas);
								peliculasTotal.insertar(Integer.parseInt(ids), actual);
							}
						}
					}

				}
			}catch(Exception e){
				System.out.println("Error cargar catalogo");
				return false;
			}
			try
			{
				JSONArray array = (JSONArray)parser.parse(new FileReader(rutaM));
				for(Object obj: array ){
					JSONObject jObject = (JSONObject) obj;

					String Id = (String) jObject.get("movie_id");
					String titulo = (String)jObject.get("title");
					String generos = (String)jObject.get("genres");

					String[] genero = generos.split("//|");

					int i=0;
					while (genero[i]!=null)
					{
						VOPelicula actual = peliculasTotal.darValor(Integer.parseInt(Id));
						ListaEncadenada<VOGeneroPelicula> generosP = actual.getGeneros();
						VOGeneroPelicula genP = new VOGeneroPelicula();
						if(listaGeneros.tieneLlave(genero[i]))
						{
							genP.setNombre(genero[i]);
							generosP.agregarElemento(genP);
							actual.setGeneros(generosP);
							actual.setTitulo(titulo);
							peliculasTotal.insertar(Integer.parseInt(Id), actual);
						}
						else
						{
							genP.setNombre(genero[i]);
							generosP.agregarElemento(genP);
							actual.setGeneros(generosP);
							actual.setTitulo(titulo);
							peliculasTotal.insertar(Integer.parseInt(Id), actual);
							listaGeneros.insertar(genero[i], genP);
						}
						i++;

					}
				}
			}
			catch(Exception e)
			{

			}
		}
		return contador == 5;
	}
	public boolean cargarRed(String ruta) {
		JSONParser parser = new JSONParser();
		int pos1 = 0;
		int pos2 = 0;
		red = new Grafo(teatros.darTamanio()); 
		try{
			JSONArray array = (JSONArray)parser.parse(new FileReader(ruta));

			for(Object obj: array ){

				JSONObject jObject = (JSONObject) obj;

				String teatro1 = (String)jObject.get("Teatro 1");
				String teatro2 = (String)jObject.get("Teatro 2");
				long tiempo = (long)jObject.get("Tiempo (minutos)");

				if(teatros.tieneLlave(teatro1)&& teatros.tieneLlave(teatro2)){
					pos1 = teatros.darValor(teatro1).getPosicion();
					pos2 = teatros.darValor(teatro2).getPosicion();
					String s = tiempo +"";
					Double t= Double.parseDouble(s);
					red.agregarArco(pos1, pos2, t);
				}
			}return true;
		}catch(Exception e){
			System.out.println("Error cargar red");
			return false;
		}
	}

	public int sizeMovies() {
		return peliculasTotal.darTamanio();
	}

	public int sizeTeatros() {
		return teatros.darTamanio();
	}

	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {

		try {
			CsvReader peliculasImport = new CsvReader("ratings_filtered.csv");
			peliculasImport.readHeaders();

			while (peliculasImport.readRecord())
			{
				VOUsuario user = new VOUsuario();
				ListaEncadenada<VORating> listaRating = new ListaEncadenada<VORating>();
				VORating ratingAgregar = new VORating();
				String id = peliculasImport.get("#user_id");
				String idMovie = peliculasImport.get("movie_id");
				String rating = peliculasImport.get("rating");

				if(usuarios.tieneLlave(Integer.parseInt(id)))
				{
					ratingAgregar.setRating(Integer.parseInt(rating));
					ratingAgregar.setIdPelicula(Integer.parseInt(idMovie));
					listaRating= usuarios.darValor(Integer.parseInt(id)).getRatings();
					listaRating.agregarElemento(ratingAgregar);
					usuarios.darValor(Integer.parseInt(id)).setRatings(listaRating);
					usuarios.insertar(Integer.parseInt(id), user);
				}
				else
				{
					user.setId(Long.parseLong(id));
					ratingAgregar.setRating(Integer.parseInt(rating));
					ratingAgregar.setIdPelicula(Integer.parseInt(idMovie));
					listaRating= user.getRatings();
					listaRating.agregarElemento(ratingAgregar);
					user.setRatings(listaRating);
					usuarios.insertar(Integer.parseInt(id), user);
				}
			}
		}
		catch (Exception e)
		{

		}

		//Lectura similitudes
		JSONParser parser = new JSONParser();
		try{
			JSONArray array = (JSONArray)parser.parse(new FileReader("data/sims.json"));

			for(Object obj: array ){

				JSONObject jObject = (JSONObject) obj;

				long idMovie = (long)jObject.get("movieId");
				String similitudes = (String) jObject.get("similitudes");
				String sim = similitudes.replace("[", " ");
				sim = similitudes.replace("]"," ");
				String[] ar = sim.split(",");
				
			}
		}
			catch (Exception e)
			{
				
			}
				
		
		return null;

	}
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero,
			VOUsuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia,
			int fecha, String franja) {
		// TODO Auto-generated method stub
		return null;
	}

	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(
			VOGeneroPelicula genero, int fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(
			VOGeneroPelicula genero, int fecha, VOFranquicia franquicia) {
		// TODO Auto-generated method stub
		return null;
	}

	public ILista<IEdge<VOTeatro>> generarMapa() {
		// TODO Auto-generated method stub
		return null;
	}

	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		// TODO Auto-generated method stub
		return null;
	}

}
