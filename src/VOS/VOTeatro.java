
package VOS;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro {
	
	/**
	* Posicion en la red
	*/
	public int posicion;
	/**
	/**
	 * Atributo que modela el nombre del teatro
	 */
	
	private String nombre;
	
	/**
	 * Atributo que modela la ubicacion del teatro 
	 */
	
	private VOUbicacion ubicacion;
	
	/**
	 * Atributo que referencia la franquicia
	 */
	
	private VOFranquicia franquicia;
	
	
	public VOTeatro() {
	// TODO Auto-generated constructor stub
	}

	public String getNombre()
	{
		return this.nombre;
	}
	public void setNombre(String nom)
	{
		nombre = nom;
	}
	
	/**
	 * Atributo que modela la ubicacion del teatro 
	 */
	public VOUbicacion getUbicacion()
	{
		return this.ubicacion;
	}
	public void setUbicacion(VOUbicacion ub)
	{
		ubicacion = ub;
	}
	
	/**
	 * Atributo que referencia la franquicia
	 */
	public VOFranquicia getFranquicia()
	{
		return this.franquicia;
	}
	public void setFranquicia(VOFranquicia fa){
		franquicia = fa;
	}
	public void setPosicion(int nuevo){
		posicion = nuevo;
		}
	
	public int getPosicion(){
		return posicion;
		}
	
}
