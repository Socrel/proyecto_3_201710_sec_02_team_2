package VOS;

import java.sql.Time;

import structures.EncadenamientoSeparadoTH;
import structures.ListaEncadenada;

public class VOPelicula {

	private int id;
	private String titulo;
	private ListaEncadenada<VOGeneroPelicula> generos;
	private EncadenamientoSeparadoTH<Integer,EncadenamientoSeparadoTH<String,ListaEncadenada<String>>> fechas = new EncadenamientoSeparadoTH<Integer,EncadenamientoSeparadoTH<String,ListaEncadenada<String>>>(50);
	private EncadenamientoSeparadoTH<String,ListaEncadenada<String>> teatros = new EncadenamientoSeparadoTH<String,ListaEncadenada<String>>(5);
	private ListaEncadenada<String> horas = new ListaEncadenada<String>();
	
	
	public VOPelicula() {
	}

	public int getId() {
		return id;
	}

	public void setId(int nuevo) {
		this.id = nuevo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String nuevo) {
		this.titulo = nuevo;
	}
	
	public ListaEncadenada<VOGeneroPelicula> getGeneros() {
		return generos;
	}

	public void setGeneros(ListaEncadenada<VOGeneroPelicula> gen) {
		this.generos = gen;
	}

	public EncadenamientoSeparadoTH<Integer,EncadenamientoSeparadoTH<String,ListaEncadenada<String>>> getDia() {
		return fechas;
	}

	public void setDia(EncadenamientoSeparadoTH<Integer,EncadenamientoSeparadoTH<String,ListaEncadenada<String>>> fecha) {
		this.fechas = fecha;
	}

	public  EncadenamientoSeparadoTH<String,ListaEncadenada<String>> getTeatros() {
		return teatros;
	}

	public void setTeatros( EncadenamientoSeparadoTH<String,ListaEncadenada<String>> nuevo) {
		this.teatros = nuevo;
	}

	public ListaEncadenada<String> getHoras() {
		return horas;
	}

	public void setHoras(ListaEncadenada<String> nuevo) {
		this.horas = nuevo;
	}
	
	
	
}
