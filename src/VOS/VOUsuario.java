package VOS;

import structures.ListaEncadenada;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario {
	
	/**
	 * Atributo que modela el id del usuario
	 */

	private long id;
/**
 * Dar lista de peliculas a las que el usuario ha hecho rating
 */
	private ListaEncadenada<VORating> ratings  = new ListaEncadenada<VORating>();
	
	
	public VOUsuario() {
		// TODO Auto-generated constructor stub
	}
	
	public long getId(){
		return id;
	}
	public void setId(long nuevo){
		id = nuevo;
	}
	
	public ListaEncadenada<VORating> getRatings()
	{
		return ratings;
	}
	public void setRatings(ListaEncadenada<VORating> rat)
	{
		this.ratings=rat;
	}
}
